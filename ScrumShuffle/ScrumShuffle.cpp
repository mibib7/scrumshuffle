﻿#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#    define ON_WINDOWS
#    define WIN32_LEAN_AND_MEAN
#    include <Windows.h>
#    include <conio.h>
#endif

#include <CLI/CLI.hpp>
#include <fmt/chrono.h>
#include <fmt/format.h>
#include <nlohmann/json.hpp>
#include <unicode/msgfmt.h>

#include <algorithm>
#include <array>
#include <atomic>
#include <cctype>
#include <chrono>
#include <condition_variable>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <random>
#include <thread>
#include <vector>

template <typename... Arguments>
icu::UnicodeString format(const icu::UnicodeString& message, Arguments... args)
{
    auto success = U_ZERO_ERROR;
    icu::UnicodeString string;
    icu::FieldPosition field_pos = 0;
    icu::MessageFormat message_format(message, success);
    std::vector<icu::Formattable> formattable_args = {
        args...,
    };

    const auto args_count = static_cast<int32_t>(formattable_args.size());
    return message_format.format(formattable_args.data(), args_count, string, field_pos, success);
}

template <typename... Arguments>
icu::UnicodeString format(const char* message, Arguments... args)
{
    const auto message_utf8 = icu::UnicodeString::fromUTF8(message);
    return format(message_utf8, args...);
}

std::ostream& operator<<(std::ostream& os, const icu::UnicodeString& string)
{
    std::string output;
    os << string.toUTF8String(output);
    return os;
}

class Timer
{
public:
    Timer() = default;

    void start()
    {
        stop();  // Ensure the old thread finished before the new one is started.
        timer_thread_ = std::jthread(&Timer::count);
    }

    void stop()
    {
        timer_thread_ = std::jthread();
    }

private:
    static void count(std::stop_token stop_token)
    {
        // Note: 500ms wakeup period to avoid the desynchronized cursor blinking.
        constexpr auto wakeup_period = std::chrono::milliseconds(500);
        const auto start = std::chrono::system_clock::now();
        auto next_wakeup = start;

        std::mutex m;
        std::unique_lock l(m);
        std::condition_variable_any cv;

        while (!stop_token.stop_requested())
        {
            const auto duration = std::chrono::system_clock::now() - start;

            const auto output = fmt::to_string(std::chrono::duration_cast<std::chrono::seconds>(duration));
            std::cout << output << std::flush;
            std::cout << fmt::format("\033[{}D", output.size());

            next_wakeup += wakeup_period;
            cv.wait_until(l, stop_token, next_wakeup, std::false_type{});
        }
    }

    std::jthread timer_thread_;
};

#if defined(ON_WINDOWS)
bool syncShouldDiscussLater()
{
    auto input = toupper(_getch());
    return input == 'Y';
}
#else
bool syncShouldDiscussLater()
{
    return false;
}
#endif

class Round
{
public:
    Round() = default;
    explicit Round(const nlohmann::json& json_round)
    {
        const auto& round_participants = json_round.at("participants");
        for (const auto& participant : round_participants)
        {
            auto participant_name = participant.get<std::string>();
            participants_.push_back(icu::UnicodeString::fromUTF8(participant_name));
        }

        const auto& round_sentences = json_round.at("sentences");
        for (const auto& sentence : round_sentences)
        {
            auto sentence_str = sentence.get<std::string>();
            sentences_.push_back(icu::UnicodeString::fromUTF8(sentence_str));
        }
    }
    ~Round() = default;

    Round play() const
    {
        std::random_device random_device;
        std::mt19937 mt_19937_generator(random_device());

        std::uniform_int_distribution<size_t> uniform_distribution(0, sentences_.size() - 1);

        std::shuffle(participants_.begin(), participants_.end(), mt_19937_generator);

        Timer timer;
        Round extra_round;
        extra_round.sentences_ = {"Extra: {0}"};

        for (const auto& participant : participants_)
        {
            auto random_index = uniform_distribution(mt_19937_generator);
            const auto& sentence = sentences_[random_index];

            std::cout << "\t" << format(sentence, participant) << " ";

            timer.start();
            if (syncShouldDiscussLater())  // Will block for any input
            {
                extra_round.participants_.push_back(participant);
            }
            timer.stop();
            std::cout << "\n\n";
        }

        return extra_round;
    }

    bool empty() const
    {
        return participants_.empty();
    }

    void flush() const
    {
        for (const auto& participant : participants_)
        {
            std::cout << "\t" << participant << "\n\n";
        }
    }

private:
    std::vector<icu::UnicodeString> sentences_;
    mutable std::vector<icu::UnicodeString> participants_;
};

class Shuffle
{
public:
    explicit Shuffle(const nlohmann::json& json)
    {
        const auto introduction_str = json.at("introduction").get<std::string>();
        introduction_ = icu::UnicodeString::fromUTF8(introduction_str);

        const auto conclusion_str = json.at("conclusion").get<std::string>();
        conclusion_ = icu::UnicodeString::fromUTF8(conclusion_str);

        const auto extra_str = json.at("extra").get<std::string>();
        extra_ = icu::UnicodeString::fromUTF8(extra_str);

        const auto& rounds = json.at("rounds");
        for (const auto& round : rounds)
        {
            rounds_.emplace_back(round);
        }
    }
    ~Shuffle() = default;

    void play() const
    {
        std::cout << introduction_ << "\n";
        std::cin.get();

        std::vector<Round> extra_rounds;

        for (auto& round : rounds_)
        {
            if (const auto extra_round = round.play(); !extra_round.empty())
            {
                extra_rounds.push_back(extra_round);
            }
        }

        if (!extra_rounds.empty())
        {
            std::cout << extra_ << "\n\n";

            for (auto& round : extra_rounds)
            {
                round.flush();
            }
        }

        std::cout << conclusion_ << "\n";
    }

private:
    icu::UnicodeString introduction_;
    icu::UnicodeString conclusion_;
    icu::UnicodeString extra_;
    std::vector<Round> rounds_;
};

nlohmann::json readJsonFile(const std::filesystem::path& file_path)
{
    if (!(std::filesystem::exists(file_path) && std::filesystem::is_regular_file(file_path)))
    {
        throw std::runtime_error(fmt::format("File not found: {}", file_path.string()));
    }

    std::ifstream in_file_stream(file_path);
    return nlohmann::json::parse(in_file_stream);
}

Shuffle makeShuffle(const std::filesystem::path& file_path)
{
    try
    {
        const auto shuffle_file_content = readJsonFile(file_path);
        return Shuffle(shuffle_file_content);
    }
    catch (const std::exception& ex)
    {
        throw std::runtime_error(
            fmt::format("The file \"{}\" could not be read. Reason: {}", file_path.string(), ex.what()));
    }
}

#if defined(ON_WINDOWS)
void setConsoleUtf8()
{
    SetConsoleOutputCP(CP_UTF8);
    setvbuf(stdout, nullptr, _IOFBF, 1000);
}

void enableANSIEscapeSequences()
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    if (handle == INVALID_HANDLE_VALUE)
    {
        return;
    }

    DWORD mode = 0;
    if (!GetConsoleMode(handle, &mode))
    {
        return;
    }

    mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    SetConsoleMode(handle, mode);
}
#else
void setConsoleUtf8()
{
}
void enableANSIEscapeSequences()
{
}
#endif

int main(int argc, char* argv[])
{
    setConsoleUtf8();
    enableANSIEscapeSequences();

    std::filesystem::path shuffle_path;

    constexpr auto version = "1.1.2";

    CLI::App cli_app{"ScrumShuffle"};
    cli_app.set_version_flag("-v, --version", version);

    auto hidden_group = cli_app.add_option_group("");
    cli_app.add_option("-s,--shuffle", shuffle_path, "Path to a JSON file that describes the Shuffle.")
        ->default_val(".\\shuffle.json");

    CLI11_PARSE(cli_app, argc, argv);

    try
    {
        const auto shuffle = makeShuffle(shuffle_path);
        shuffle.play();
    }
    catch (const std::exception& ex)
    {
        std::cout << ex.what() << "\n";
    }

    return 0;
}
